package cl.renta.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import cl.renta.model.Auto;

public interface AutoDao extends CrudRepository<Auto,Long> {

	public List<Auto> findByCategoria(String categoria);
	
}
