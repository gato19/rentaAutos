package cl.renta.dao;

import org.springframework.data.repository.CrudRepository;
import cl.renta.model.Cliente;


public interface  ClienteDao extends CrudRepository<Cliente,Long>{
	
	public boolean findByRut(String rut);
	
}

