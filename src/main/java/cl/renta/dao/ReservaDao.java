package cl.renta.dao;

import org.springframework.data.repository.CrudRepository;
import cl.renta.model.Reserva;

public interface  ReservaDao extends CrudRepository<Reserva,Long>{
		
}
