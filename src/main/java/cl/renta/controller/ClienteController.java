package cl.renta.controller;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cl.renta.model.Cliente;
import cl.renta.service.ClienteService;

@RequestMapping("/cliente")
@RestController
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	
	@RequestMapping (value = "/crearCliente", method = POST)
	@ResponseBody
	public ResponseEntity<Cliente>CrearToDo(@RequestBody Cliente cliente){
		clienteService.registrarCliente(cliente);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.CREATED);
		
	}
}
