package cl.renta.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Auto {
	
	@Id
	@GeneratedValue
	private long id;
	private String marca;
	private String modelo;
	private String categoria;
	private String tipo_transmision;
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the marca
	 */
	public String getMarca() {
		return marca;
	}
	/**
	 * @param marca the marca to set
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}
	/**
	 * @return the modelo
	 */
	public String getModelo() {
		return modelo;
	}
	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	/**
	 * @return the categoria
	 */
	public String getCategoria() {
		return categoria;
	}
	/**
	 * @param categoría the categoria to set
	 */
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	/**
	 * @return the tipo_transmisión
	 */
	public String getTipo_transmision() {
		return tipo_transmision;
	}
	/**
	 * @param tipo_transmisión the tipo_transmisión to set
	 */
	public void setTipo_transmision(String tipo_transmision) {
		this.tipo_transmision = tipo_transmision;
	}
	
}
