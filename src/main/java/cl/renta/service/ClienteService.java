package cl.renta.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import cl.renta.dao.ClienteDao;
import cl.renta.model.Cliente;
import cl.renta.service.ClienteService;

public class ClienteService {

	@Autowired
	private static ClienteDao clienteDao;
	
	@Autowired
	public ClienteService(ClienteDao clienteDao){
		ClienteService.clienteDao=clienteDao;
	}

	public Cliente registrarCliente(Cliente cliente) {
		
		return clienteDao.save(cliente);
	}
	
	public static List<ArrayList<Object>> obtenerTodosLosClientes() {

		List<ArrayList<Object>> listaClientes = new ArrayList<ArrayList<Object>>();
		List<Cliente> Clientes = new ArrayList<Cliente>();
		
		Clientes =(List<Cliente>) clienteDao.findAll(); 
		for(int i=0;i<Clientes.size();i++)
		{
			ArrayList<Object> cliente = new ArrayList<Object>();
			cliente.add(Clientes.get(i).getRut());
			cliente.add(Clientes.get(i).getNombre());
			cliente.add(Clientes.get(i).getNumero_celular());
			listaClientes.add(cliente); 
		}
		
		return listaClientes;
	}


	public boolean buscarCliente(String rut) {
		
		return clienteDao.findByRut(rut);
	}
}
