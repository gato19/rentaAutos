package cl.renta.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cl.renta.dao.AutoDao;
import cl.renta.model.Auto;

@Service
public class AutoService {
	
	@Autowired
	private AutoDao autodao;
	
	public List<ArrayList<String>> obtenerTodosLosAutosPorCategoria(String categoria) {
		// TODO Auto-generated method stub
		List<ArrayList<String>> misAutos = new ArrayList<ArrayList<String>>();
		List<Auto> AutosCategoria = new ArrayList<Auto>();
		
		AutosCategoria =(List<Auto>) autodao.findByCategoria(categoria); 
		for(int i=0;i<AutosCategoria.size();i++)
		{
			ArrayList<String> Autito = new ArrayList<String>();
			Autito.add(AutosCategoria.get(i).getMarca());
			Autito.add(AutosCategoria.get(i).getModelo());
			Autito.add(AutosCategoria.get(i).getTipo_transmision());
			misAutos.add(Autito); 
		}
		
		return misAutos;
	}
	
}
