package cl.renta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoRentaAutos {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoRentaAutos.class, args);
	}
}
