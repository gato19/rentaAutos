package cl.renta;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import cl.renta.dao.AutoDao;
import cl.renta.model.Auto;
import cl.renta.service.AutoService;

@RunWith(MockitoJUnitRunner.class)
public class AutoServicesTest {

	@Mock
	private AutoDao autodao;
	
	@InjectMocks
	private AutoService autoservice;
	
	@Test
	public void listarTodosLosAutosPorCategoriaV2(){
		//arrange
		List<Auto> misAutos = new ArrayList<Auto>();
		List<ArrayList<String>> resultado = new ArrayList<ArrayList<String>>();
		
		//act
		when(autodao.findByCategoria(anyString())).thenReturn(misAutos);
		resultado = autoservice.obtenerTodosLosAutosPorCategoria("Camioneta");
		
		//assert
		Assert.assertNotNull(resultado);
		for(int i=0;i<misAutos.size();i++)
		{
			Assert.assertEquals(misAutos.get(i).getMarca(),resultado.get(i).get(0));
			Assert.assertEquals(misAutos.get(i).getModelo(),resultado.get(i).get(1));
			Assert.assertEquals(misAutos.get(i).getTipo_transmision(),resultado.get(i).get(2));
		}

	}

}
