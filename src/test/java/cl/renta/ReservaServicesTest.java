package cl.renta;

import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import cl.renta.dao.ReservaDao;
import cl.renta.model.Auto;
import cl.renta.model.Cliente;
import cl.renta.model.Reserva;
import cl.renta.service.ReservaService;

@RunWith(MockitoJUnitRunner.class)
public class ReservaServicesTest {

	@Mock
	private ReservaDao reservaDao;
	
	@InjectMocks
	private ReservaService reservaService;
	
	@Test
	public void reservarUnAuto(){
		//arrange
		Cliente cliente=new Cliente();
		Auto auto=new Auto();
		Reserva reserva=new Reserva();
		ArrayList<Object> resultado = new ArrayList<Object>();

		reserva.setId_automovil(auto.getId());
		reserva.setId_cliente(cliente.getId());
		
		//act 
		when(reservaDao.save(reserva)).thenReturn(reserva);
		resultado=ReservaService.realizarReserva(cliente.getId(),reserva.getFecha_inicio(),reserva.getFecha_fin(),"Lujo");
		
		//assert 
		Assert.assertNotNull(resultado);
		Assert.assertEquals(reserva.getId(), resultado.get(0));
		Assert.assertEquals(reserva.getId_automovil(),resultado.get(1));
		Assert.assertEquals(reserva.getFecha_inicio(),resultado.get(2));
		Assert.assertEquals(reserva.getFecha_fin(),resultado.get(3));
		
	}
}
