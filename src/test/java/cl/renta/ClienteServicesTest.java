package cl.renta;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import cl.renta.dao.ClienteDao;
import cl.renta.model.Cliente;
import cl.renta.service.ClienteService;

@RunWith(MockitoJUnitRunner.class)
public class ClienteServicesTest {

	@Mock
	private ClienteDao clienteDao;
	
	@InjectMocks
	private ClienteService clienteService;
	
	@Test
	public void registrarUnCliente(){
		//arrange
		Cliente cliente=new Cliente();
		
		//act
		when(clienteDao.save(cliente)).thenReturn(cliente);
		Cliente clienteCreado = clienteService.registrarCliente(cliente);
		
		//assert 
		Assert.assertNotNull(clienteCreado);
		Assert.assertEquals(cliente, clienteCreado);
	}
	
	@Test
	public void listarTodosLosClientes(){
		//arrange
		List<Cliente> misClientes = new ArrayList<Cliente>();
		List<ArrayList<Object>> resultado = new ArrayList<ArrayList<Object>>();
		
		//act
		when(clienteDao.findAll()).thenReturn(misClientes);
		resultado = ClienteService.obtenerTodosLosClientes();
		
		//assert
		Assert.assertNotNull(resultado);
		for(int i=0;i<misClientes.size();i++)
		{
			Assert.assertEquals(misClientes.get(i).getRut(),resultado.get(i).get(0));
			Assert.assertEquals(misClientes.get(i).getNombre(),resultado.get(i).get(1));
			Assert.assertEquals(misClientes.get(i).getNumero_celular(),resultado.get(i).get(2));
		}

	}
	
	@Test
	public void consultaSiClienteEstaRegistrado(){
		//arrange
		Cliente cliente=new Cliente();
		cliente.setRut("17640423-K");
		boolean resultadoEsperado=true;
		boolean resultado;
		
		//act 
		when(clienteDao.findByRut("17640423-K")).thenReturn(resultadoEsperado);
		resultado=clienteService.buscarCliente(cliente.getRut());

		//assert
		Assert.assertTrue(resultado);
		Assert.assertNotNull(resultado);
		Assert.assertEquals(resultadoEsperado, resultado);
	}
	
}
