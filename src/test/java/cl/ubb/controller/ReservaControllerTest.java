package cl.ubb.controller;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

import cl.renta.controller.ReservaController;
import cl.renta.model.Reserva;
import cl.renta.service.ReservaService;

@RunWith(MockitoJUnitRunner.class)
public class ReservaControllerTest {

	@Mock
	private ReservaService reservaService;
	
	@InjectMocks
	private ReservaController reservaController;
	
	@Test
	public void registraCliente() {
		RestAssuredMockMvc.standaloneSetup(reservaController);
		Reserva reserva = new Reserva();
		reserva.setId(1l);
		reserva.setFecha_inicio(null);
		reserva.setFecha_fin(null);
		
		Mockito.when(reservaService.realizarReserva(1l,reserva.getFecha_inicio(),reserva.getFecha_fin(),"lujo")).thenReturn(reserva);
		
		given().
			contentType("application/json").
			body(reserva).
		when().
			post("/reserva/registraCliente").
		then().
			body("Id", equalTo(reserva.getId())).
			statusCode(201);
	}

}
