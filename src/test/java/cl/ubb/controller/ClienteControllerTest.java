package cl.ubb.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


import cl.renta.controller.ClienteController;
import cl.renta.model.Cliente;
import cl.renta.service.ClienteService;
import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;



@RunWith(MockitoJUnitRunner.class)
public class ClienteControllerTest {

	@Mock
	private ClienteService clienteService;
	
	@InjectMocks
	private ClienteController clienteController;

	@Test
	public void registraCliente() {
		RestAssuredMockMvc.standaloneSetup(clienteController);
		Cliente cliente = new Cliente();
		cliente.setRut("17.234.534-k");
		cliente.setNombre("Pablo Cruz Cea");
		cliente.setNumero_celular(983283283);
		cliente.setEmail("pabcruz@gmail.com");
		
		Mockito.when(clienteService.registrarCliente(Matchers.any(Cliente.class))).thenReturn(cliente);
		
		given().
			contentType("application/json").
			body(cliente).
		when().
			post("/cliente/registraCliente").
		then().
			body("Nombre", equalTo(cliente.getNombre())).
			statusCode(201);
	}
	
}
